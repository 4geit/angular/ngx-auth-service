import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import 'rxjs/add/observable/of';
import 'rxjs/add/observable/fromPromise';
import 'rxjs/add/operator/switchMap';
import { CookieService } from 'ngx-cookie';

import { NgxPageService } from '@4geit/ngx-page-service';
import { NgxSwaggerClientService } from '@4geit/ngx-swagger-client-service';

@Injectable()
export class NgxAuthService {

  isLoggedIn = false;

  // store the URL so we can redirect after logging in
  redirectUrl: string;

  // store account info
  account: any;

  constructor(
    private router: Router,
    private cookie: CookieService,
    private swaggerClientService: NgxSwaggerClientService,
    private pageService: NgxPageService
  ) { }

  restoreSession(): Observable<boolean> {
    // already logged in
    if (this.isLoggedIn) { return Observable.of(true); }
    const token = this.cookie.get('token');

    // tokens doesnt exist
    if (!token) { return Observable.of(false); }
    // token exist restore session
    // not loaded yet
    // fix issue https://github.com/angular/angular/issues/6005
    setTimeout(() => {
      this.pageService.isLoaded = false;
    });

    // return observable from promise
    return Observable.fromPromise(
      this.swaggerClientService.buildClient()
        // set token to swagger client
        .then(() => this.swaggerClientService.buildClientWithToken(token))
        // call the account endpoint from the swagger client
        .then(() => this.swaggerClientService.client.apis.Account.getAccount())
    )
      .map((res: any) => {
        const _account = res.body;

        // store the account object to this.account
        this.account = _account;

        // switch `isLoggedIn` to true
        this.isLoggedIn = true;

        // loaded
        // fix issue https://github.com/angular/angular/issues/6005
        setTimeout(() => {
          this.pageService.isLoaded = true;
        });

        // and return true
        return Observable.of(true);
      })
      .catch(err => {
        // reset account value
        this.account = undefined;

        // remove cookie
        this.cookie.remove('token');

        // switch `isLoggedIn` to false
        this.isLoggedIn = false;

        // loaded
        // fix issue https://github.com/angular/angular/issues/6005
        setTimeout(() => {
          this.pageService.isLoaded = true;
        });

        // remove the token from the swagger client service
        this.swaggerClientService.buildClient();

        // redirect to the login form
        this.router.navigate(['/login']);

        // return false
        return Observable.throw(err);
      })
    ;
  }

  login(account, rememberMe?: boolean) {
    // call the login endpoint from the swagger client
    return Observable.fromPromise(
      this.swaggerClientService.client.apis.Account.login({
        account: account
      })
    )
      .switchMap((res: any) => {
        const _account = res.body;

        // if remember me enabled, create the token cookie token with the relevant token value
        if (rememberMe) {
          this.cookie.put('token', _account.token);
        }

        // store object to this.account
        this.account = _account;

        return Observable.fromPromise(
          // set token to swagger client service
          this.swaggerClientService.buildClientWithToken(_account.token)
            .then(() => {
              // set isLoggedIn to true
              this.isLoggedIn = true;
            })
        );
      })
    ;
  }

  logout(): void {
    // reset data
    this.isLoggedIn = false;
    this.account = undefined;
    // remove apikeys from api services
    // remove the token from the swagger client service
    this.swaggerClientService.buildClient();
    // remove tokens from cookies
    this.cookie.remove('token');
    // redirect to login
    this.router.navigate(['/login']);
  }

}
