import { TestBed, inject } from '@angular/core/testing';

import { NgxAuthService } from './ngx-auth.service';

describe('NgxAuthService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [NgxAuthService]
    });
  });

  it('should ...', inject([NgxAuthService], (service: NgxAuthService) => {
    expect(service).toBeTruthy();
  }));
});
