import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CookieModule } from 'ngx-cookie';

import { NgxAuthService } from './ngx-auth.service';

@NgModule({
  imports: [
    CommonModule,
    CookieModule.forRoot()
  ],
  providers: [
    NgxAuthService
  ],
  exports: [
  ]
})
export class NgxAuthModule { }
