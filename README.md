# @4geit/ngx-auth-service [![npm version](//badge.fury.io/js/@4geit%2Fngx-auth-service.svg)](//badge.fury.io/js/@4geit%2Fngx-auth-service)

---

add an authentication service based on a swagger client

## Installation

1. A recommended way to install ***@4geit/ngx-auth-service*** is through [npm](//www.npmjs.com/search?q=@4geit/ngx-auth-service) package manager using the following command:

```bash
npm i @4geit/ngx-auth-service --save
```

Or use `yarn` using the following command:

```bash
yarn add @4geit/ngx-auth-service
```

2. You need to import the `NgxAuthService` service in the module you want to use it. For instance `app.module.ts` as follows:

```js
import { NgxAuthService } from '@4geit/ngx-auth-service';
```

And you also need to add the `NgxAuthService` service within the `@NgModule` decorator as part of the `providers` list.

```js
@NgModule({
  // ...
  providers: [
    // ...
    NgxAuthService,
    // ...
  ],
  // ...
})
export class AppModule { }
```

3. This service allows to override some variables. You can change a variable value as shown in the example below. First you will need to import those variables:

```js
import { VAR1 } from '@4geit/ngx-auth-service';
```

And set the variable value in the `providers` list as below:

```js
@NgModule({
  // ...
  providers: [
    // ...
    { provide: VAR1, useValue: 'VALUE1' },
    // ...
  ],
  // ...
})
export class AppModule { }
```

Here are the list of the variables this service allows you to override:

* VAR1
* VAR2
* VAR3
